provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "example_bucket" {
  bucket = "example-bucket-name"
  acl    = "public-read" # This line intentionally allows public read access

  tags = {
    Name        = "ExampleBucket"
    Environment = "Dev"
  }
}
